package com.example.library.book.rest.restbook.controller;

import com.example.library.book.rest.restbook.RestBookApplication;
import com.example.library.book.rest.restbook.config.JpaConfig;
import com.example.library.book.rest.restbook.payload.BookPayload;
import com.example.library.book.rest.restbook.util.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(value = SpringRunner.class)
@SpringBootTest(classes = {
    RestBookApplication.class,
    JpaConfig.class
})
@ActiveProfiles("test")
public class BookControllerTest {

  private static final String BASE_URI_API = "/book";

  @Autowired
  private WebApplicationContext webApplicationContext;

  private MockMvc mockMvc;

  @Before
  public void before() {
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
  }

  private BookPayload buildDefaultPayload() {
    final BookPayload payload = new BookPayload();

    payload.setPublishingYear(1991L);
    payload.setEdition(1L);
    payload.setAvailableQuantity(2L);
    payload.setExistenceQuantity(2L);

    payload.setEditorialId(4L);

    final Set<Long> refIds = Stream.of(4L, 5L).collect(Collectors.toSet());

    payload.setAuthors(refIds);

    payload.setGenres(refIds);

    return payload;
  }

  @Test
  public void givenValidPayload_WhenCreateBook_ThenReturnIsOk() throws Exception {
    final BookPayload payload = buildDefaultPayload();
    payload.setTitle("Dummy Title Created " + System.currentTimeMillis());

    this.mockMvc.perform(post(BASE_URI_API)
        .content(Utils.OBJECT_MAPPER.writeValueAsString(payload))
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenInvalidPayload_WhenCreateBook_ThenReturnIsBadRequest() throws Exception {
    final BookPayload payload = new BookPayload();

    this.mockMvc.perform(post(BASE_URI_API)
        .content(Utils.OBJECT_MAPPER.writeValueAsString(payload))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void givenValidPayload_WhenUpdateBook_ThenReturnIsOk() throws Exception {
    final BookPayload payload = buildDefaultPayload();

    payload.setTitle("Dummy Book Updated");

    this.mockMvc.perform(put(BASE_URI_API + "/{id}", 1L)
        .content(Utils.OBJECT_MAPPER.writeValueAsString(payload))
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenValidPayload_WhenUpdateBook_WithWrongId_ThenReturnIsBadRequest() throws Exception {
    final BookPayload payload = buildDefaultPayload();

    payload.setTitle("Dummy Book Updated");

    this.mockMvc.perform(put(BASE_URI_API + "/{id}", -1L)
        .content(Utils.OBJECT_MAPPER.writeValueAsString(payload))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void givenInvalidPayload_WhenUpdateBook_WithWrongId_ThenReturnIsBadRequest() throws Exception {
    final BookPayload payload = new BookPayload();

    this.mockMvc.perform(put(BASE_URI_API + "/{id}", -1L)
        .content(Utils.OBJECT_MAPPER.writeValueAsString(payload))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void givenValidParams_WhenListBooks_ThenReturnIsOk() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API)
        .param("keyword", "dummy")
        .param("pageNumber", Long.valueOf(0L).toString())
        .param("pageSize", Long.valueOf(10L).toString())
        .param("order", Long.valueOf(-1L).toString())
        .param("orderByField", "id")
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenValidParams_WhenListBooks_WithStatusActive_ThenReturnIsOk() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API)
        .param("status", Boolean.TRUE.toString())
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenValidParams_WhenListBooks_WithStatusDisable_ThenReturnIsOk() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API)
        .param("status", Boolean.FALSE.toString())
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenInvalidKeyword_WhenListBooks_ThenReturnIsOkEmpty() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API)
        .param("keyword", "NO_EXISTING_KEYWORD")
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string("[]"));
  }

  @Test
  public void givenValidId_WhenGetBookById_ThenReturnIsOk() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API + "/{id}", 3L)
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenInvalidId_WhenGetBookById_ThenReturnIsNotFound() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API + "/{id}", -1L)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }

  @Test
  public void givenValidId_WhenEnableBook_ThenReturnIsOk() throws Exception {
    this.mockMvc.perform(put(BASE_URI_API + "/{id}/enable", 2L)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void givenValidId_WhenDisableBook_ThenReturnIsOk() throws Exception {
    this.mockMvc.perform(put(BASE_URI_API + "/{id}/disable", 4L)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void givenValidParams_WhenUpdateBookAvailableQuantity_ThenReturnIsOk() throws Exception {
    this.mockMvc.perform(put(BASE_URI_API + "/{id}/available-quantity", 5L)
        .param("quantityToAdd", Long.valueOf(-1).toString())
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }
}
