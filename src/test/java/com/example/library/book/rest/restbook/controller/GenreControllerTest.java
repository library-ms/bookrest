package com.example.library.book.rest.restbook.controller;


import com.example.library.book.rest.restbook.RestBookApplication;
import com.example.library.book.rest.restbook.config.JpaConfig;
import com.example.library.book.rest.restbook.payload.GenrePayload;
import com.example.library.book.rest.restbook.util.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(value = SpringRunner.class)
@SpringBootTest(classes = {
    RestBookApplication.class,
    JpaConfig.class
})
@ActiveProfiles("test")
public class GenreControllerTest {

  private static final String BASE_URI_API = "/genre";

  @Autowired
  private WebApplicationContext webApplicationContext;

  private MockMvc mockMvc;

  @Before
  public void before() {
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
  }

  @Test
  public void givenValidPayload_WhenCreateGenre_ThenReturnIsOk() throws Exception {
    final GenrePayload payload = new GenrePayload();

    payload.setName("Dummy Genre Created " + System.currentTimeMillis());
    payload.setDescription("Dummy Genre Description");

    this.mockMvc.perform(post(BASE_URI_API)
        .content(Utils.OBJECT_MAPPER.writeValueAsString(payload))
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenInvalidPayload_WhenCreateGenre_ThenReturnIsBadRequest() throws Exception {
    final GenrePayload payload = new GenrePayload();

    this.mockMvc.perform(post(BASE_URI_API)
        .content(Utils.OBJECT_MAPPER.writeValueAsString(payload))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void givenValidPayload_WhenUpdateGenre_ThenReturnIsOk() throws Exception {
    final GenrePayload payload = new GenrePayload();
    payload.setName("Dummy Genre Updated");
    payload.setDescription("-");

    this.mockMvc.perform(put(BASE_URI_API + "/{id}", 1L)
        .content(Utils.OBJECT_MAPPER.writeValueAsString(payload))
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenValidPayload_WhenUpdateGenre_WithWrongId_ThenReturnIsBadRequest() throws Exception {
    final GenrePayload payload = new GenrePayload();
    payload.setName("Dummy Genre Updated");
    payload.setDescription("-");

    this.mockMvc.perform(put(BASE_URI_API + "/{id}", -1L)
        .content(Utils.OBJECT_MAPPER.writeValueAsString(payload))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void givenInvalidPayload_WhenUpdateGenre_WithWrongId_ThenReturnIsBadRequest() throws Exception {
    final GenrePayload payload = new GenrePayload();

    this.mockMvc.perform(put(BASE_URI_API + "/{id}", -1L)
        .content(Utils.OBJECT_MAPPER.writeValueAsString(payload))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void givenValidId_WhenDeleteGenre_ThenReturnIsOk() throws Exception {
    this.mockMvc.perform(delete(BASE_URI_API + "/{id}", 10L)
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenInvalidId_WhenDeleteGenre_ThenReturnIsOkEmpty() throws Exception {
    this.mockMvc.perform(delete(BASE_URI_API + "/{id}", -1L)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void givenNoParams_WhenListGenres_ThenReturnIsOk() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API)
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenValidParams_WhenListGenres_ThenReturnIsOk() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API)
        .param("keyword", "dummy")
        .param("pageNumber", Long.valueOf(0L).toString())
        .param("pageSize", Long.valueOf(10L).toString())
        .param("order", Long.valueOf(-1L).toString())
        .param("orderByField", "id")
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenInvalidParams_WhenListGenres_ThenReturnIsOkEmpty() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API)
        .param("keyword", "INVALID_KEYWORD")
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string("[]"));
  }

  @Test
  public void givenValidId_WhenGetGenreById_ThenReturnIsOk() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API + "/{id}", 3L)
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenInvalidId_WhenGetGenreById_ThenReturnIsNotFound() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API + "/{id}", -1L)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }
}
