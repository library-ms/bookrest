package com.example.library.book.rest.restbook.controller;


import com.example.library.book.rest.restbook.RestBookApplication;
import com.example.library.book.rest.restbook.config.JpaConfig;
import com.example.library.book.rest.restbook.payload.AuthorPayload;
import com.example.library.book.rest.restbook.util.Utils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(value = SpringRunner.class)
@SpringBootTest(classes = {
    RestBookApplication.class,
    JpaConfig.class
})
@ActiveProfiles("test")
public class AuthorControllerTest {

  private static final String BASE_URI_API = "/author";

  @Autowired
  private WebApplicationContext webApplicationContext;

  private MockMvc mockMvc;

  @Before
  public void before() {
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
  }

  @Test
  public void givenValidPayload_WhenCreateAuthor_ThenReturnIsOk() throws Exception {
    final AuthorPayload payload = new AuthorPayload();

    payload.setName("Alan Moore " + System.currentTimeMillis());

    this.mockMvc.perform(post(BASE_URI_API)
        .content(Utils.OBJECT_MAPPER.writeValueAsString(payload))
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenInvalidPayload_WhenCreateAuthor_ThenReturnIsBadRequest() throws Exception {
    final AuthorPayload payload = new AuthorPayload();

    this.mockMvc.perform(post(BASE_URI_API)
        .content(Utils.OBJECT_MAPPER.writeValueAsString(payload))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void givenValidPayload_WhenUpdateAuthor_ThenReturnIsOk() throws Exception {
    final AuthorPayload payload = new AuthorPayload();
    payload.setName("Dummy Author Updated");

    this.mockMvc.perform(put(BASE_URI_API + "/{id}", 1L)
        .content(Utils.OBJECT_MAPPER.writeValueAsString(payload))
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenValidPayload_WhenUpdateAuthor_WithWrongId_ThenReturnIsBadRequest() throws Exception {
    final AuthorPayload payload = new AuthorPayload();
    payload.setName("Dummy Author Updated");

    this.mockMvc.perform(put(BASE_URI_API + "/{id}", -1L)
        .content(Utils.OBJECT_MAPPER.writeValueAsString(payload))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void givenInvalidPayload_WhenUpdateAuthor_WithWrongId_ThenReturnIsBadRequest() throws Exception {
    final AuthorPayload payload = new AuthorPayload();

    this.mockMvc.perform(put(BASE_URI_API + "/{id}", -1L)
        .content(Utils.OBJECT_MAPPER.writeValueAsString(payload))
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void givenValidId_WhenDeleteAuthor_ThenReturnIsOk() throws Exception {
    this.mockMvc.perform(delete(BASE_URI_API + "/{id}", 10L)
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenInvalidId_WhenDeleteAuthor_ThenReturnIsOkEmpty() throws Exception {
    this.mockMvc.perform(delete(BASE_URI_API + "/{id}", -1L)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void givenNoParams_WhenListAuthors_ThenReturnIsOk() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API)
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenValidParams_WhenListAuthors_ThenReturnIsOk() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API)
        .param("keyword", "dummy")
        .param("pageNumber", Long.valueOf(0L).toString())
        .param("pageSize", Long.valueOf(10L).toString())
        .param("order", Long.valueOf(-1L).toString())
        .param("orderByField", "id")
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenInvalidParams_WhenListAuthors_ThenReturnIsOkEmpty() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API)
        .param("keyword", "INVALID_KEYWORD")
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string("[]"));
  }

  @Test
  public void givenValidId_WhenGetAuthorById_ThenReturnIsOk() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API + "/{id}", 3L)
        .contentType(MediaType.APPLICATION_JSON))
        .andDo(print())
        .andExpect(status().isOk());
  }

  @Test
  public void givenInvalidId_WhenGetAuthorById_ThenReturnIsNotFound() throws Exception {
    this.mockMvc.perform(get(BASE_URI_API + "/{id}", -1L)
        .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isNotFound());
  }
}
