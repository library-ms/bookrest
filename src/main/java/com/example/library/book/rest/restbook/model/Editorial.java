package com.example.library.book.rest.restbook.model;

import com.example.library.book.rest.restbook.dto.EditorialDTO;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "editorial")
public class Editorial implements IModel<EditorialDTO> {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", nullable = false)
  @NotBlank
  private String name;

  public EditorialDTO buildDTO() {
    return EditorialDTO.builder()
        .id(id)
        .name(name)
        .build();
  }
}
