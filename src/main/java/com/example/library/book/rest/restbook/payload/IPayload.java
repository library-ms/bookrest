package com.example.library.book.rest.restbook.payload;

import java.io.Serializable;

public interface IPayload<T> extends Serializable {
  T buildModel();
}
