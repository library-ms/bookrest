package com.example.library.book.rest.restbook.repository;

import com.example.library.book.rest.restbook.model.BookAuthor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IBookAuthorRepository extends JpaRepository<BookAuthor, Long> {

  void deleteAllByBook_Id(final Long bookId);
}
