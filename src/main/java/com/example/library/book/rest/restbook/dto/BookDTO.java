package com.example.library.book.rest.restbook.dto;

import lombok.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookDTO implements Serializable {
  private Long id;
  private String title;
  private Boolean active = true;
  private Long existenceQuantity = 0L;
  private Long availableQuantity = 0L;
  private Long publishingYear;
  private Long edition;
  private EditorialDTO editorial;
  private Set<AuthorDTO> authors = new HashSet<>();
  private Set<GenreDTO> genres = new HashSet<>();

}
