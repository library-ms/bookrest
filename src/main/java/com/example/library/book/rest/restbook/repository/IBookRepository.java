package com.example.library.book.rest.restbook.repository;

import com.example.library.book.rest.restbook.model.Book;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IBookRepository extends JpaRepository<Book, Long> {

  Optional<Book> getBookByTitle(final String title);

  Optional<Book> getBookById(final Long id);

  @Query("Select b From Book b " +
      "Left Join fetch b.editorial e " +
      "Left Join Fetch b.genres g " +
      "Left Join Fetch g.genre ge " +
      "Left Join Fetch b.authors a " +
      "Left Join Fetch a.author au " +
      "Where b.id = ?1")
  Optional<Book> getBookByIdFetched(final Long id);

  @Query("Select b From Book b " +
      "Left Join fetch b.editorial e " +
      "Left Join Fetch b.genres g " +
      "Left Join Fetch g.genre ge " +
      "Left Join Fetch b.authors a " +
      "Left Join Fetch a.author au " +
      "Where ( ?1 is null " +
      "or UPPER(b.title) Like %?1% " +
      "or UPPER(e.name) Like %?1% " +
      "or UPPER(ge.name) Like %?1% " +
      "or UPPER(au.name) Like %?1% ) " +
      "And ( ?2 is null or b.active = ?2 )")
  List<Book> findAllByKeyword(final String keyword, final Boolean status, final Pageable pageable);
}
