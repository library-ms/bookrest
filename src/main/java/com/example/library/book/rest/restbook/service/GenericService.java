package com.example.library.book.rest.restbook.service;

import com.example.library.book.rest.restbook.model.IModel;
import com.example.library.book.rest.restbook.repository.IGenericRepository;
import com.example.library.book.rest.restbook.util.Utils;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class GenericService<T extends IModel> {

  private IGenericRepository<T> iRepository;

  public GenericService(IGenericRepository<T> iRepository) {
    this.iRepository = iRepository;
  }

  public ResponseEntity<String> list(
      final String keyword,
      final Long pageNumber,
      final Long pageSize,
      final Integer order,
      final String orderByField
  ) throws Exception {

    final Pageable paginationAndSorting = Utils.buildPageable(
        pageNumber,
        pageSize,
        order,
        orderByField
    );

    final List<T> rows = iRepository.findAllByKeyword(keyword == null || keyword.trim().isEmpty() ? null : keyword.trim().toUpperCase(), paginationAndSorting);

    return ResponseEntity.ok(
        Utils.OBJECT_MAPPER.writeValueAsString(
            rows.stream().map(T::buildDTO)
                .collect(Collectors.toList())));
  }

  List<T> getRows(final List<Long> ids) {
    if (!ids.isEmpty()) {
      return iRepository.findAllById(ids);
    }

    return Collections.emptyList();
  }
}
