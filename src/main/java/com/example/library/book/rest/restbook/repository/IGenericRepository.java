package com.example.library.book.rest.restbook.repository;

import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IGenericRepository<T> {

  List<T> findAllByKeyword(final String keyword, final Pageable pageable);

  List<T> findAllById(final Iterable<Long> ids);
}
