package com.example.library.book.rest.restbook.controller;

import com.example.library.book.rest.restbook.payload.AuthorPayload;
import com.example.library.book.rest.restbook.service.AuthorService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("author")
public class AuthorController {

  private AuthorService authorService;

  @Autowired
  public AuthorController(AuthorService authorService) {
    this.authorService = authorService;
  }

  @GetMapping
  public ResponseEntity<String> listAuthors(
      @ApiParam(name = "keyword", value = "Author name keywords") @RequestParam(required = false) final String keyword,
      @RequestParam(defaultValue = "0") final Long pageNumber,
      @RequestParam(defaultValue = "10") final Long pageSize,
      @ApiParam(name = "order", value = "Order results[1 ASC,-1 DESC]") @RequestParam(defaultValue = "0") final Integer order,
      @RequestParam(required = false) final String orderByField
  ) {
    try {
      return authorService.list(keyword, pageNumber, pageSize, order, orderByField);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @PostMapping
  public ResponseEntity<String> createAuthor(
      @RequestBody final AuthorPayload payload
  ) {
    try {
      return authorService.create(payload);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @PutMapping("{id}")
  public ResponseEntity<String> updateAuthor(
      @PathVariable final Long id,
      @RequestBody final AuthorPayload payload
  ) {
    try {
      return authorService.update(id, payload);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @DeleteMapping("{id}")
  public ResponseEntity<String> deleteAuthor(
      @PathVariable final Long id
  ) {
    try {
      return authorService.delete(id);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @GetMapping("{id}")
  public ResponseEntity<String> getAuthorById(
      @PathVariable final Long id
  ) {
    try {
      return authorService.getById(id);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }
}
