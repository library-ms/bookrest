package com.example.library.book.rest.restbook.service;

import com.example.library.book.rest.restbook.constants.ErrorMessageConstants;
import com.example.library.book.rest.restbook.model.Author;
import com.example.library.book.rest.restbook.payload.AuthorPayload;
import com.example.library.book.rest.restbook.repository.IAuthorRepository;
import com.example.library.book.rest.restbook.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthorService extends GenericService<Author> {

  private IAuthorRepository authorRepository;

  @Autowired
  public AuthorService(IAuthorRepository authorRepository) {
    super(authorRepository);
    this.authorRepository = authorRepository;
  }

  private boolean existsById(final Long id) {
    return authorRepository.getAuthorById(id).isPresent();
  }

  private void assertNotExists(
      final AuthorPayload payload
  ) throws Exception {
    final Optional<Author> author = authorRepository.getAuthorByName(payload.getName().trim());

    if (author.isPresent()) {
      throw new Exception(ErrorMessageConstants.ERROR_AUTHOR_ALREADY_EXISTS);
    }
  }

  private void assertExists(
      final Long id
  ) throws Exception {
    if (!existsById(id)) {
      throw new Exception(ErrorMessageConstants.ERROR_AUTHOR_DOESNT_EXISTS);
    }
  }

  public ResponseEntity<String> create(
      final AuthorPayload payload
  ) throws Exception {
    Utils.assertValidPayload(payload);

    assertNotExists(payload);

    final Author author = authorRepository.saveAndFlush(payload.buildModel());

    return ResponseEntity.ok(Utils.OBJECT_MAPPER.writeValueAsString(author.buildDTO()));
  }

  public ResponseEntity<String> update(
      final Long id,
      final AuthorPayload payload
  ) throws Exception {
    Utils.assertValidPayload(payload);

    assertExists(id);

    final Author author = payload.buildModel();
    author.setId(id);

    authorRepository.save(author);

    return ResponseEntity.ok(Utils.OBJECT_MAPPER.writeValueAsString(author.buildDTO()));
  }

  public ResponseEntity<String> delete(
      final Long id
  ) {
    if (existsById(id)) {
      authorRepository.deleteById(id);
    }

    return ResponseEntity.ok().build();
  }

  public ResponseEntity<String> getById(
      final Long id
  ) throws Exception {
    final Optional<Author> author = authorRepository.getAuthorById(id);

    if (author.isPresent()) {
      return ResponseEntity.ok(Utils.OBJECT_MAPPER.writeValueAsString(author.get().buildDTO()));
    }

    return ResponseEntity.notFound().build();
  }
}
