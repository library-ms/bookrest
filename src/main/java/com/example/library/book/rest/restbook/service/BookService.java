package com.example.library.book.rest.restbook.service;

import com.example.library.book.rest.restbook.constants.ErrorMessageConstants;
import com.example.library.book.rest.restbook.model.Author;
import com.example.library.book.rest.restbook.model.Book;
import com.example.library.book.rest.restbook.model.Editorial;
import com.example.library.book.rest.restbook.model.Genre;
import com.example.library.book.rest.restbook.payload.BookPayload;
import com.example.library.book.rest.restbook.repository.IBookRepository;
import com.example.library.book.rest.restbook.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BookService {

  private IBookRepository bookRepository;
  private BookAuthorService bookAuthorService;
  private AuthorService authorService;
  private BookGenreService bookGenreService;
  private GenreService genreService;
  private EditorialService editorialService;

  @Autowired
  public BookService(
      IBookRepository bookRepository,
      BookAuthorService bookAuthorService,
      AuthorService authorService,
      BookGenreService bookGenreService,
      GenreService genreService,
      EditorialService editorialService
  ) {
    this.bookRepository = bookRepository;
    this.bookAuthorService = bookAuthorService;
    this.authorService = authorService;
    this.bookGenreService = bookGenreService;
    this.genreService = genreService;
    this.editorialService = editorialService;
  }

  private void assertNotExists(
      final BookPayload payload
  ) throws Exception {
    final Optional<Book> book = bookRepository.getBookByTitle(payload.getTitle().trim());

    if (book.isPresent()) {
      throw new Exception(ErrorMessageConstants.ERROR_BOOK_ALREADY_EXISTS);
    }
  }

  private void assertExists(
      final Long id
  ) throws Exception {
    final Optional<Book> book = bookRepository.getBookById(id);

    if (!book.isPresent()) {
      throw new Exception(ErrorMessageConstants.ERROR_BOOK_DOESNT_EXISTS);
    }
  }

  private Book saveBook(
      final Long id,
      final Book book,
      final Editorial editorial
  ) {
    book.setId(id);

    book.setEditorial(editorial);

    return bookRepository.save(book);
  }

  public ResponseEntity<String> create(
      final BookPayload payload
  ) throws Exception {

    return createOrUpdateBook(
        null,
        payload
    );
  }

  @Transactional
  public ResponseEntity<String> update(
      final Long id,
      final BookPayload payload
  ) throws Exception {

    return createOrUpdateBook(
        id,
        payload
    );
  }

  private ResponseEntity<String> createOrUpdateBook(
      final Long id,
      final BookPayload payload
  ) throws Exception {
    Utils.assertValidPayload(payload);

    if (id == null) {
      assertNotExists(payload);
    } else {
      assertExists(id);
    }

    final List<Author> authors = authorService.getRows(new ArrayList<>(payload.getAuthors()));

    final List<Genre> genres = genreService.getRows(new ArrayList<>(payload.getGenres()));

    final Editorial editorial = editorialService.getEditorial(payload.getEditorialId());

    if (id != null) {
      bookAuthorService.deleteAllByBookId(id);

      bookGenreService.deleteAllByBookId(id);
    }

    final Book book = saveBook(
        id,
        payload.buildModel(),
        editorial
    );

    bookAuthorService.createBookAuthors(
        book,
        authors
    );

    bookGenreService.createBookGenres(
        book,
        genres
    );

    return ResponseEntity.ok(
        Utils.OBJECT_MAPPER.writeValueAsString(
            book.buildDTO(
                new HashSet<>(authors),
                new HashSet<>(genres),
                editorial
            )
        )
    );
  }

  public void enableBook(
      final Long id
  ) throws Exception {
    changeBookStatus(
        id,
        true
    );
  }

  public void disableBook(
      final Long id
  ) throws Exception {
    changeBookStatus(
        id,
        false
    );
  }

  private void changeBookStatus(
      final Long id,
      final boolean status
  ) throws Exception {
    assertExists(id);

    final Optional<Book> optionalBook = bookRepository.getBookById(id);

    if (!optionalBook.isPresent()) {
      return;
    }

    final Book book = optionalBook.get();

    if ((status && !Boolean.TRUE.equals(book.getActive())) || (!status && !Boolean.FALSE.equals(book.getActive()))) {

      book.setActive(status);

      bookRepository.save(book);
    }
  }

  public ResponseEntity<String> getById(
      final Long id
  ) throws Exception {
    final Optional<Book> optionalBook = bookRepository.getBookByIdFetched(id);

    if (!optionalBook.isPresent()) {
      return ResponseEntity.notFound().build();
    }

    final Book book = optionalBook.get();

    return ResponseEntity.ok(
        Utils.OBJECT_MAPPER.writeValueAsString(
            book.buildDTO()));
  }

  public ResponseEntity<String> list(
      final String keyword,
      final Boolean status,
      final Long pageNumber,
      final Long pageSize,
      final Integer order,
      final String orderByField
  ) throws Exception {

    final Pageable paginationAndSorting = Utils.buildPageable(
        pageNumber,
        pageSize,
        order,
        orderByField
    );

    final List<Book> books = bookRepository.findAllByKeyword(keyword == null || keyword.trim().isEmpty() ?
        null : keyword.trim().toUpperCase(), status, paginationAndSorting);

    return ResponseEntity.ok(
        Utils.OBJECT_MAPPER.writeValueAsString(
            books.stream().map(Book::buildDTO)
                .collect(Collectors.toList())));
  }

  public void updateBookAvailableQuantity(
      final Long id,
      final Long quantityToAdd
  ) throws Exception {
    assertExists(id);

    final Optional<Book> optionalBook = bookRepository.getBookById(id);

    if (!optionalBook.isPresent()) {
      return;
    }

    final Book book = optionalBook.get();

    book.setAvailableQuantity(book.getAvailableQuantity() + quantityToAdd);

    bookRepository.save(book);
  }
}
