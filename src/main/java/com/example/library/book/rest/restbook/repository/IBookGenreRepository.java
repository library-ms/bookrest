package com.example.library.book.rest.restbook.repository;

import com.example.library.book.rest.restbook.model.BookGenre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IBookGenreRepository extends JpaRepository<BookGenre, Long> {

  void deleteAllByBook_Id(final Long bookId);
}
