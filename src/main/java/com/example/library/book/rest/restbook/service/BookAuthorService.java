package com.example.library.book.rest.restbook.service;

import com.example.library.book.rest.restbook.model.Author;
import com.example.library.book.rest.restbook.model.Book;
import com.example.library.book.rest.restbook.model.BookAuthor;
import com.example.library.book.rest.restbook.repository.IBookAuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookAuthorService {

  private IBookAuthorRepository bookAuthorRepository;

  @Autowired
  public BookAuthorService(IBookAuthorRepository bookAuthorRepository) {
    this.bookAuthorRepository = bookAuthorRepository;
  }

  List<BookAuthor> createBookAuthors(
      final Book book,
      final List<Author> authors
  ) {

    if (authors.isEmpty()) {
      return Collections.emptyList();
    }

    final List<BookAuthor> bookAuthors =
        authors.stream()
            .distinct()
            .map(author -> new BookAuthor(null, book, author))
            .collect(Collectors.toList());

    return bookAuthorRepository.saveAll(bookAuthors);
  }

  void deleteAllByBookId(
      final Long bookId
  ) {
    if (bookId == null || bookId < 1) {
      return;
    }

    bookAuthorRepository.deleteAllByBook_Id(bookId);
  }
}
