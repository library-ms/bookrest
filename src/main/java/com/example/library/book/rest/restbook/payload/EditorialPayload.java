package com.example.library.book.rest.restbook.payload;

import com.example.library.book.rest.restbook.model.Editorial;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode
@NoArgsConstructor
public class EditorialPayload implements IPayload<Editorial> {
  @NotBlank
  private String name;

  public Editorial buildModel() {
    return Editorial.builder()
        .name(name.trim())
        .build();
  }
}
