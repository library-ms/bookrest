package com.example.library.book.rest.restbook.model;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "book_author")
public class BookAuthor {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @OneToOne(cascade = CascadeType.REFRESH)
  @JoinColumn(name = "book_id", referencedColumnName = "id")
  private Book book;

  @OneToOne(cascade = CascadeType.REFRESH)
  @JoinColumn(name = "author_id", referencedColumnName = "id")
  private Author author;
}
