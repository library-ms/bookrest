package com.example.library.book.rest.restbook.model;

import lombok.*;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "book_genre")
public class BookGenre {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @OneToOne(cascade = CascadeType.REFRESH)
  @JoinColumn(name = "book_id", referencedColumnName = "id")
  private Book book;

  @OneToOne(cascade = CascadeType.REFRESH)
  @JoinColumn(name = "genre_id", referencedColumnName = "id")
  private Genre genre;
}
