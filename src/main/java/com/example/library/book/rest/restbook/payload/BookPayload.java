package com.example.library.book.rest.restbook.payload;

import com.example.library.book.rest.restbook.model.Book;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Data
@EqualsAndHashCode
@NoArgsConstructor
public class BookPayload implements IPayload<Book> {
  @NotBlank
  private String title;
  @NotNull
  private Boolean active = true;
  @NotNull
  private Long existenceQuantity = 0L;
  @NotNull
  private Long availableQuantity = 0L;
  private Long publishingYear;
  private Long edition;
  private Long editorialId;

  private Set<Long> authors = new HashSet<>();
  private Set<Long> genres = new HashSet<>();

  public Book buildModel() {
    return Book.builder()
        .title(title)
        .active(active)
        .existenceQuantity(existenceQuantity)
        .availableQuantity(availableQuantity)
        .publishingYear(publishingYear)
        .edition(edition)
        .build();
  }
}
