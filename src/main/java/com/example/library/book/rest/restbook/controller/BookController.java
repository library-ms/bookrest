package com.example.library.book.rest.restbook.controller;

import com.example.library.book.rest.restbook.payload.BookPayload;
import com.example.library.book.rest.restbook.service.BookService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("book")
public class BookController {

  private BookService bookService;

  @Autowired
  public BookController(BookService bookService) {
    this.bookService = bookService;
  }

  @GetMapping
  public ResponseEntity<String> listBooks(
      @ApiParam(name = "keyword", value = "Book title, editorial, author names, genre names keywords")
      @RequestParam(required = false) final String keyword,
      @ApiParam(name = "status", value = "Book status[true: Actives, false: disabled, null: All]")
      @RequestParam(required = false) final Boolean status,
      @RequestParam(defaultValue = "0") final Long pageNumber,
      @RequestParam(defaultValue = "10") final Long pageSize,
      @ApiParam(name = "order", value = "Order results[1 ASC,-1 DESC]") @RequestParam(defaultValue = "0") final Integer order,
      @RequestParam(required = false) final String orderByField
  ) {
    try {
      return bookService.list(keyword, status, pageNumber, pageSize, order, orderByField);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @PostMapping
  public ResponseEntity<String> createBook(
      @RequestBody BookPayload payload
  ) {
    try {
      return bookService.create(payload);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @PutMapping("{id}")
  public ResponseEntity<String> updateBook(
      @PathVariable final Long id,
      @RequestBody BookPayload payload
  ) {
    try {
      return bookService.update(id, payload);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @GetMapping("{id}")
  public ResponseEntity<String> getBookById(
      @PathVariable final Long id
  ) {
    try {
      return bookService.getById(id);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @PutMapping("{id}/enable")
  public ResponseEntity<String> enableBook(
      @PathVariable final Long id
  ) {
    try {
      bookService.enableBook(id);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
    return ResponseEntity.ok().build();
  }

  @PutMapping("{id}/disable")
  public ResponseEntity<String> disableBook(
      @PathVariable final Long id
  ) {
    try {
      bookService.disableBook(id);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
    return ResponseEntity.ok().build();
  }

  @PutMapping("{id}/available-quantity")
  public ResponseEntity<String> updateBookAvailableQuantity(
      @PathVariable final Long id,
      @RequestParam final Long quantityToAdd
  ) {
    try {
      bookService.updateBookAvailableQuantity(
          id,
          quantityToAdd
      );
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
    return ResponseEntity.ok().build();
  }
}
