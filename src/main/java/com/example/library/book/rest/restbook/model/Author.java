package com.example.library.book.rest.restbook.model;

import com.example.library.book.rest.restbook.dto.AuthorDTO;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "author")
public class Author implements IModel<AuthorDTO> {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", nullable = false)
  @NotBlank
  private String name;

  public AuthorDTO buildDTO() {
    return AuthorDTO.builder()
        .id(id)
        .name(name)
        .build();
  }
}
