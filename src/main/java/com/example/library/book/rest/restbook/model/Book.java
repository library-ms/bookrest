package com.example.library.book.rest.restbook.model;

import com.example.library.book.rest.restbook.dto.BookDTO;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "book")
public class Book implements IModel<BookDTO> {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "title", nullable = false)
  @NotBlank
  private String title;

  @Column(name = "active", nullable = false)
  @NotNull
  private Boolean active = true;

  @Column(name = "existence_quantity", nullable = false)
  @NotNull
  @Builder.Default
  private Long existenceQuantity = 0L;

  @Column(name = "available_quantity", nullable = false)
  @NotNull
  @Builder.Default
  private Long availableQuantity = 0L;

  @Column(name = "publishing_year")
  private Long publishingYear;

  @Column(name = "edition")
  private Long edition;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "editorial_id", referencedColumnName = "id")
  @EqualsAndHashCode.Exclude
  private Editorial editorial;

  @OneToMany(mappedBy = "book")
  @Builder.Default
  @EqualsAndHashCode.Exclude
  private Set<BookAuthor> authors = new HashSet<>();

  @OneToMany(mappedBy = "book")
  @Builder.Default
  @EqualsAndHashCode.Exclude
  private Set<BookGenre> genres = new HashSet<>();

  public BookDTO buildDTO(
      final Set<Author> authors,
      final Set<Genre> genres,
      final Editorial editorial
  ) {
    final BookDTO bookDTO = BookDTO.builder()
        .id(id)
        .title(title.trim())
        .active(active)
        .existenceQuantity(existenceQuantity)
        .availableQuantity(availableQuantity)
        .publishingYear(publishingYear)
        .edition(edition)
        .build();

    bookDTO.setEditorial(editorial.buildDTO());

    bookDTO.setAuthors(
        authors.stream()
            .map(Author::buildDTO)
            .collect(Collectors.toSet()));

    bookDTO.setGenres(
        genres.stream()
            .map(Genre::buildDTO)
            .collect(Collectors.toSet()));

    return bookDTO;
  }

  public BookDTO buildDTO() {
    return buildDTO(
        authors.stream()
            .map(BookAuthor::getAuthor)
            .collect(Collectors.toSet()),
        genres.stream()
            .map(BookGenre::getGenre)
            .collect(Collectors.toSet()),
        editorial
    );
  }
}
