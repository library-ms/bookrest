package com.example.library.book.rest.restbook.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.validation.Valid;

public class Utils {

  public static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

  private Utils() {
  }

  public static void assertValidPayload(@Valid final Object object) throws Exception {
  }

  public static Pageable buildPageable(
      final Long pageNumber,
      final Long pageSize,
      final Integer order,
      final String orderByField
  ) {
    final Pageable paginationAndSorting;

    if (orderByField == null || orderByField.trim().isEmpty()) {
      paginationAndSorting = PageRequest.of(pageNumber.intValue(), pageSize.intValue());
    } else {
      paginationAndSorting = PageRequest.of(pageNumber.intValue(), pageSize.intValue(),
          order < 0 ? Sort.by(orderByField).descending() : Sort.by(orderByField));
    }

    return paginationAndSorting;
  }
}
