package com.example.library.book.rest.restbook.repository;

import com.example.library.book.rest.restbook.model.Editorial;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IEditorialRepository extends JpaRepository<Editorial, Long>, IGenericRepository<Editorial> {

  Optional<Editorial> getEditorialByName(final String name);

  Optional<Editorial> getEditorialById(final Long id);

  @Query("select e from Editorial e Where ?1 is null or UPPER(e.name) like %?1%")
  List<Editorial> findAllByKeyword(final String keyword, final Pageable pageable);
}
