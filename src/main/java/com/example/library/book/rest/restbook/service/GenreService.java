package com.example.library.book.rest.restbook.service;

import com.example.library.book.rest.restbook.constants.ErrorMessageConstants;
import com.example.library.book.rest.restbook.model.Genre;
import com.example.library.book.rest.restbook.payload.GenrePayload;
import com.example.library.book.rest.restbook.repository.IGenreRepository;
import com.example.library.book.rest.restbook.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GenreService extends GenericService<Genre> {

  private IGenreRepository genreRepository;

  @Autowired
  public GenreService(IGenreRepository genreRepository) {
    super(genreRepository);
    this.genreRepository = genreRepository;
  }

  private boolean existsById(final Long id) {
    return genreRepository.getGenreById(id).isPresent();
  }

  private void assertNotExists(
      final GenrePayload payload
  ) throws Exception {
    final Optional<Genre> genre = genreRepository.getGenreByName(payload.getName().trim());

    if (genre.isPresent()) {
      throw new Exception(ErrorMessageConstants.ERROR_GENRE_ALREADY_EXISTS);
    }
  }

  private void assertExists(
      final Long id
  ) throws Exception {
    if (!existsById(id)) {
      throw new Exception(ErrorMessageConstants.ERROR_GENRE_DOESNT_EXISTS);
    }
  }

  public ResponseEntity<String> create(
      final GenrePayload payload
  ) throws Exception {
    Utils.assertValidPayload(payload);

    assertNotExists(payload);

    final Genre genre = genreRepository.save(payload.buildModel());

    return ResponseEntity.ok(Utils.OBJECT_MAPPER.writeValueAsString(genre.buildDTO()));
  }

  public ResponseEntity<String> update(
      final Long id,
      final GenrePayload payload
  ) throws Exception {
    Utils.assertValidPayload(payload);

    assertExists(id);

    final Genre genre = payload.buildModel();
    genre.setId(id);

    genreRepository.save(genre);

    return ResponseEntity.ok(Utils.OBJECT_MAPPER.writeValueAsString(genre.buildDTO()));
  }

  public ResponseEntity<String> delete(
      final Long id
  ) {
    if (existsById(id)) {
      genreRepository.deleteById(id);
    }

    return ResponseEntity.ok().build();
  }

  public ResponseEntity<String> getById(
      final Long id
  ) throws Exception {
    final Optional<Genre> genre = genreRepository.getGenreById(id);

    if (genre.isPresent()) {
      return ResponseEntity.ok(Utils.OBJECT_MAPPER.writeValueAsString(genre.get().buildDTO()));
    }

    return ResponseEntity.notFound().build();
  }
}
