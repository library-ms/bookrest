package com.example.library.book.rest.restbook.model;

import com.example.library.book.rest.restbook.dto.GenreDTO;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@Builder
@Entity
@Table(name = "genre")
public class Genre implements IModel<GenreDTO> {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", nullable = false)
  @NotBlank
  private String name;

  @Column(name = "description")
  private String description;

  public GenreDTO buildDTO() {
    return GenreDTO.builder()
        .id(id)
        .name(name)
        .description(description)
        .build();
  }
}
