package com.example.library.book.rest.restbook.constants;

public class ErrorMessageConstants {

  private ErrorMessageConstants() {
  }

  public static final String ERROR_AUTHOR_ALREADY_EXISTS = "Author Already Exists";
  public static final String ERROR_AUTHOR_DOESNT_EXISTS = "Author Doesn't Exists";

  public static final String ERROR_GENRE_ALREADY_EXISTS = "Genre Already Exists";
  public static final String ERROR_GENRE_DOESNT_EXISTS = "Genre Doesn't Exists";

  public static final String ERROR_EDITORIAL_ALREADY_EXISTS = "Editorial Already Exists";
  public static final String ERROR_EDITORIAL_DOESNT_EXISTS = "Editorial Doesn't Exists";

  public static final String ERROR_BOOK_ALREADY_EXISTS = "Book Already Exists";
  public static final String ERROR_BOOK_DOESNT_EXISTS = "Book Doesn't Exists";
}
