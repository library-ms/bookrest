package com.example.library.book.rest.restbook.dto;

import lombok.*;

import java.io.Serializable;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AuthorDTO implements Serializable {
  private Long id;
  private String name;
}
