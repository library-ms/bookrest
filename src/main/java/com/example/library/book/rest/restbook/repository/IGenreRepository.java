package com.example.library.book.rest.restbook.repository;

import com.example.library.book.rest.restbook.model.Genre;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IGenreRepository extends JpaRepository<Genre, Long>, IGenericRepository<Genre> {

  Optional<Genre> getGenreByName(final String name);

  Optional<Genre> getGenreById(final Long id);

  @Query("select g from Genre g Where ?1 is null or UPPER(g.name) like %?1% or UPPER(g.description) like %?1%")
  List<Genre> findAllByKeyword(final String keyword, final Pageable pageable);
}
