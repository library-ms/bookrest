package com.example.library.book.rest.restbook.controller;

import com.example.library.book.rest.restbook.payload.EditorialPayload;
import com.example.library.book.rest.restbook.service.EditorialService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("editorial")
public class EditorialController {

  private EditorialService editorialService;

  @Autowired
  public EditorialController(EditorialService editorialService) {
    this.editorialService = editorialService;
  }

  @GetMapping
  public ResponseEntity<String> listEditorials(
      @ApiParam(name = "keyword", value = "Editorial name keywords") @RequestParam(required = false) final String keyword,
      @RequestParam(defaultValue = "0") final Long pageNumber,
      @RequestParam(defaultValue = "10") final Long pageSize,
      @ApiParam(name = "order", value = "Order results[1 ASC,-1 DESC]") @RequestParam(defaultValue = "0") final Integer order,
      @RequestParam(required = false) final String orderByField
  ) {
    try {
      return editorialService.list(keyword, pageNumber, pageSize, order, orderByField);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @PostMapping
  public ResponseEntity<String> createEditorial(
      @RequestBody final EditorialPayload payload
  ) {
    try {
      return editorialService.create(payload);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @PutMapping("{id}")
  public ResponseEntity<String> updateEditorial(
      @PathVariable final Long id,
      @RequestBody final EditorialPayload payload
  ) {
    try {
      return editorialService.update(id, payload);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @DeleteMapping("{id}")
  public ResponseEntity<String> deleteEditorial(
      @PathVariable final Long id
  ) {
    try {
      return editorialService.delete(id);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @GetMapping("{id}")
  public ResponseEntity<String> getEditorialById(
      @PathVariable final Long id
  ) {
    try {
      return editorialService.getById(id);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }
}
