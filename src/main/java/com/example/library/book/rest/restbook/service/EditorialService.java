package com.example.library.book.rest.restbook.service;

import com.example.library.book.rest.restbook.constants.ErrorMessageConstants;
import com.example.library.book.rest.restbook.model.Editorial;
import com.example.library.book.rest.restbook.payload.EditorialPayload;
import com.example.library.book.rest.restbook.repository.IEditorialRepository;
import com.example.library.book.rest.restbook.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class EditorialService extends GenericService<Editorial> {

  private IEditorialRepository editorialRepository;

  @Autowired
  public EditorialService(IEditorialRepository editorialRepository) {
    super(editorialRepository);
    this.editorialRepository = editorialRepository;
  }

  private boolean existsById(final Long id) {
    return editorialRepository.getEditorialById(id).isPresent();
  }

  private void assertNotExists(
      final EditorialPayload payload
  ) throws Exception {
    final Optional<Editorial> editorial = editorialRepository.getEditorialByName(payload.getName().trim());

    if (editorial.isPresent()) {
      throw new Exception(ErrorMessageConstants.ERROR_EDITORIAL_ALREADY_EXISTS);
    }
  }

  private void assertExists(
      final Long id
  ) throws Exception {
    if (!existsById(id)) {
      throw new Exception(ErrorMessageConstants.ERROR_EDITORIAL_DOESNT_EXISTS);
    }
  }

  public ResponseEntity<String> create(
      final EditorialPayload payload
  ) throws Exception {
    Utils.assertValidPayload(payload);

    assertNotExists(payload);

    final Editorial editorial = editorialRepository.save(payload.buildModel());

    return ResponseEntity.ok(Utils.OBJECT_MAPPER.writeValueAsString(editorial.buildDTO()));
  }

  public ResponseEntity<String> update(
      final Long id,
      final EditorialPayload payload
  ) throws Exception {
    Utils.assertValidPayload(payload);

    assertExists(id);

    final Editorial editorial = payload.buildModel();
    editorial.setId(id);

    editorialRepository.save(editorial);

    return ResponseEntity.ok(Utils.OBJECT_MAPPER.writeValueAsString(editorial.buildDTO()));
  }

  public ResponseEntity<String> delete(
      final Long id
  ) {
    if (existsById(id)) {
      editorialRepository.deleteById(id);
    }

    return ResponseEntity.ok().build();
  }

  public ResponseEntity<String> getById(
      final Long id
  ) throws Exception {
    final Optional<Editorial> editorial = editorialRepository.getEditorialById(id);

    if (editorial.isPresent()) {
      return ResponseEntity.ok(Utils.OBJECT_MAPPER.writeValueAsString(editorial.get().buildDTO()));
    }

    return ResponseEntity.notFound().build();
  }

  Editorial getEditorial(final Long id) {
    if (id == null || id < 1) {
      return null;
    }

    return editorialRepository.getEditorialById(id)
        .orElse(null);
  }
}
