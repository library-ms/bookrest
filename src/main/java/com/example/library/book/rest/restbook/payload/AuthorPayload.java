package com.example.library.book.rest.restbook.payload;

import com.example.library.book.rest.restbook.model.Author;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode
@NoArgsConstructor
public class AuthorPayload implements IPayload<Author> {
  @NotBlank
  private String name;

  public Author buildModel() {
    return Author.builder()
        .name(name.trim())
        .build();
  }
}
