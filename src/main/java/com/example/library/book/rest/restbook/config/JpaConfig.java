package com.example.library.book.rest.restbook.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.util.Objects;
import java.util.Properties;

@Configuration
@EnableJpaAuditing
@EnableJpaRepositories({"com.example.library.book.rest.restbook.repository"})
public class JpaConfig {

  private Environment env;

  @Bean
  public LocalContainerEntityManagerFactoryBean entityManagerFactory(
      @Qualifier("dataSource") DataSource dataSource
  ) {
    LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
    em.setDataSource(dataSource);
    em.setPackagesToScan("com.example.library.book.rest.restbook.model");
    em.setJpaVendorAdapter(jpaVendorAdapter());
    em.setJpaProperties(jpaProperties());
    return em;
  }

  private Properties jpaProperties() {
    Properties jpaProperties = new Properties();

    jpaProperties.put("hibernate.dialect", "org.hibernate.dialect.MySQL5Dialect");
    jpaProperties.put("hibernate.enable_lazy_load_no_trans", true);
    jpaProperties.put("hibernate.jdbc.lob.non_contextual_creation", true);
    jpaProperties.put("hibernate.show_sql", Objects.requireNonNull(env.getProperty("spring.jpa.show-sql")));
    jpaProperties.put("hibernate.format_sql", Objects.requireNonNull(env.getProperty("spring.jpa.properties.hibernate.format_sql")));

    return jpaProperties;
  }

  @Bean
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(
        Objects.requireNonNull(env.getProperty("spring.datasource.driver-class-name")));
    dataSource.setUrl(env.getProperty("spring.datasource.url"));
    dataSource.setUsername(env.getProperty("spring.datasource.username"));
    dataSource.setPassword(env.getProperty("spring.datasource.password"));

    return dataSource;
  }

  @Bean
  public JpaVendorAdapter jpaVendorAdapter() {
    HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
    hibernateJpaVendorAdapter.setShowSql(Boolean.TRUE);
    hibernateJpaVendorAdapter.setDatabase(Database.MYSQL);
    return hibernateJpaVendorAdapter;
  }

  @Autowired
  public void setEnv(Environment env) {
    this.env = env;
  }
}
