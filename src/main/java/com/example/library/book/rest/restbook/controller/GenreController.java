package com.example.library.book.rest.restbook.controller;

import com.example.library.book.rest.restbook.payload.GenrePayload;
import com.example.library.book.rest.restbook.service.GenreService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("genre")
public class GenreController {

  private GenreService genreService;

  @Autowired
  public GenreController(GenreService genreService) {
    this.genreService = genreService;
  }

  @GetMapping
  public ResponseEntity<String> listGenres(
      @ApiParam(name = "keyword", value = "Genre name or description keywords") @RequestParam(required = false) final String keyword,
      @RequestParam(defaultValue = "0") final Long pageNumber,
      @RequestParam(defaultValue = "10") final Long pageSize,
      @ApiParam(name = "order", value = "Order results[1 ASC,-1 DESC]") @RequestParam(defaultValue = "0") final Integer order,
      @RequestParam(required = false) final String orderByField
  ) {
    try {
      return genreService.list(keyword, pageNumber, pageSize, order, orderByField);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @PostMapping
  public ResponseEntity<String> createGenre(
      @RequestBody final GenrePayload payload
  ) {
    try {
      return genreService.create(payload);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @PutMapping("{id}")
  public ResponseEntity<String> updateGenre(
      @PathVariable final Long id,
      @RequestBody final GenrePayload payload
  ) {
    try {
      return genreService.update(id, payload);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @DeleteMapping("{id}")
  public ResponseEntity<String> deleteGenre(
      @PathVariable final Long id
  ) {
    try {
      return genreService.delete(id);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }

  @GetMapping("{id}")
  public ResponseEntity<String> getGenreById(
      @PathVariable final Long id
  ) {
    try {
      return genreService.getById(id);
    } catch (Exception e) {
      return ResponseEntity.badRequest().body(e.getMessage());
    }
  }
}
