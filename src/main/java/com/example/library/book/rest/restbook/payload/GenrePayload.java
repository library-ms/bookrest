package com.example.library.book.rest.restbook.payload;

import com.example.library.book.rest.restbook.model.Genre;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode
@NoArgsConstructor
public class GenrePayload implements IPayload<Genre> {
  @NotBlank
  private String name;

  private String description;

  public Genre buildModel() {
    return Genre.builder()
        .name(name.trim())
        .description(description)
        .build();
  }
}
