package com.example.library.book.rest.restbook.dto;

import lombok.*;

import java.io.Serializable;

@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GenreDTO implements Serializable {
  private Long id;
  private String name;
  private String description;
}