package com.example.library.book.rest.restbook.repository;

import com.example.library.book.rest.restbook.model.Author;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface IAuthorRepository extends JpaRepository<Author, Long>, IGenericRepository<Author> {

  Optional<Author> getAuthorByName(final String name);

  Optional<Author> getAuthorById(final Long id);

  @Query("select a from Author a Where ?1 is null or UPPER(a.name) like %?1%")
  List<Author> findAllByKeyword(final String keyword, final Pageable pageable);
}
