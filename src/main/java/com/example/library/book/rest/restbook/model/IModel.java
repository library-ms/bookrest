package com.example.library.book.rest.restbook.model;

public interface IModel<T> {
  Long getId();

  T buildDTO();
}
