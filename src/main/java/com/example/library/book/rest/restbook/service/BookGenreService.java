package com.example.library.book.rest.restbook.service;

import com.example.library.book.rest.restbook.model.Book;
import com.example.library.book.rest.restbook.model.BookGenre;
import com.example.library.book.rest.restbook.model.Genre;
import com.example.library.book.rest.restbook.repository.IBookGenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BookGenreService {

  private IBookGenreRepository bookGenreRepository;

  @Autowired
  public BookGenreService(IBookGenreRepository bookGenreRepository) {
    this.bookGenreRepository = bookGenreRepository;
  }

  List<BookGenre> createBookGenres(
      final Book book,
      final List<Genre> genres
  ) {

    if (genres.isEmpty()) {
      return Collections.emptyList();
    }

    final List<BookGenre> bookGenres =
        genres.stream()
            .distinct()
            .map(genre -> new BookGenre(null, book, genre))
            .collect(Collectors.toList());

    return bookGenreRepository.saveAll(bookGenres);
  }

  void deleteAllByBookId(
      final Long bookId
  ) {
    if (bookId == null || bookId < 1) {
      return;
    }

    bookGenreRepository.deleteAllByBook_Id(bookId);
  }
}
