# Library Books Micro-service

Java SpringBoot based Micro-Service with Swagger-ui implementation.
Database Versioning with Liquibase.
Dependency Injection & JUnit code coverage.

Please note that this is a Demo project for portfolio purposes.


## pre-requirements

* IDE for Java 
* Java JDK 8+
* MySQL


## Project Setup

1. Clone the project
2. Open with your Java IDE
3. Resolve Maven Dependencies
4. Create Database library_db 
5. Update liquibase/liquibase.properties and src/main/resources/application.properties with the local database properties
6. Run line command: liquibase update  
7. Run JUnit Tests 
8. Run the project and Go to the [Swagger-UI](http://localhost/8095/swagger-ui.html) to explore the WebServices.

When is all set, should look like this:

![Swagger-UI_Screen-Shot](./ScreenShot.0.0.1.jpg)

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)