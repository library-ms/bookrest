--liquibase formatted sql

--changeset lisandro.ortega:library_db.author.changelog.0.2 context:test
--comment populate table author
INSERT INTO library_db.author (name) VALUES('Dummy Author 1');
INSERT INTO library_db.author (name) VALUES('Dummy Author 2');
INSERT INTO library_db.author (name) VALUES('Dummy Author 3');
INSERT INTO library_db.author (name) VALUES('Dummy Author 4');
INSERT INTO library_db.author (name) VALUES('Dummy Author 5');
INSERT INTO library_db.author (name) VALUES('Dummy Author 6');
INSERT INTO library_db.author (name) VALUES('Dummy Author 7');
INSERT INTO library_db.author (name) VALUES('Dummy Author 8');
INSERT INTO library_db.author (name) VALUES('Dummy Author 9');
INSERT INTO library_db.author (name) VALUES('Dummy Author 10');
--rollback

--changeset lisandro.ortega:library_db.genre.changelog.0.2 context:test
--comment populate table genre
INSERT INTO library_db.genre (name, description) VALUES('Dummy Genre 1', 'Dummy Genre Description');
INSERT INTO library_db.genre (name, description) VALUES('Dummy Genre 2', 'Dummy Genre Description');
INSERT INTO library_db.genre (name, description) VALUES('Dummy Genre 3', 'Dummy Genre Description');
INSERT INTO library_db.genre (name, description) VALUES('Dummy Genre 4', 'Dummy Genre Description');
INSERT INTO library_db.genre (name, description) VALUES('Dummy Genre 5', 'Dummy Genre Description');
INSERT INTO library_db.genre (name, description) VALUES('Dummy Genre 6', 'Dummy Genre Description');
INSERT INTO library_db.genre (name, description) VALUES('Dummy Genre 7', 'Dummy Genre Description');
INSERT INTO library_db.genre (name, description) VALUES('Dummy Genre 8', 'Dummy Genre Description');
INSERT INTO library_db.genre (name, description) VALUES('Dummy Genre 9', 'Dummy Genre Description');
INSERT INTO library_db.genre (name, description) VALUES('Dummy Genre 10', 'Dummy Genre Description');
--rollback

--changeset lisandro.ortega:library_db.editorial.changelog.0.2 context:test
--comment populate table editorial
INSERT INTO library_db.editorial (name) VALUES('Dummy Editorial 1');
INSERT INTO library_db.editorial (name) VALUES('Dummy Editorial 2');
INSERT INTO library_db.editorial (name) VALUES('Dummy Editorial 3');
INSERT INTO library_db.editorial (name) VALUES('Dummy Editorial 4');
INSERT INTO library_db.editorial (name) VALUES('Dummy Editorial 5');
INSERT INTO library_db.editorial (name) VALUES('Dummy Editorial 6');
INSERT INTO library_db.editorial (name) VALUES('Dummy Editorial 7');
INSERT INTO library_db.editorial (name) VALUES('Dummy Editorial 8');
INSERT INTO library_db.editorial (name) VALUES('Dummy Editorial 9');
INSERT INTO library_db.editorial (name) VALUES('Dummy Editorial 10');
--rollback

--changeset lisandro.ortega:library_db.book.changelog.0.2 context:test
--comment populate table book
INSERT INTO library_db.book (title, active, existence_quantity, available_quantity, publishing_year, edition, editorial_id) VALUES('Dummy Book 1', 0, 1, 1, 2001, 9, 1);
INSERT INTO library_db.book (title, active, existence_quantity, available_quantity, publishing_year, edition, editorial_id) VALUES('Dummy Book 2', 1, 2, 2, 2002, 8, 2);
INSERT INTO library_db.book (title, active, existence_quantity, available_quantity, publishing_year, edition, editorial_id) VALUES('Dummy Book 3', 0, 3, 3, 2003, 7, 3);
INSERT INTO library_db.book (title, active, existence_quantity, available_quantity, publishing_year, edition, editorial_id) VALUES('Dummy Book 4', 1, 4, 4, 2004, 6, 1);
INSERT INTO library_db.book (title, active, existence_quantity, available_quantity, publishing_year, edition, editorial_id) VALUES('Dummy Book 5', 0, 5, 5, 2005, 5, 2);
INSERT INTO library_db.book (title, active, existence_quantity, available_quantity, publishing_year, edition, editorial_id) VALUES('Dummy Book 6', 1, 6, 6, 2006, 4, 3);
INSERT INTO library_db.book (title, active, existence_quantity, available_quantity, publishing_year, edition, editorial_id) VALUES('Dummy Book 7', 0, 7, 7, 2007, 3, 1);
INSERT INTO library_db.book (title, active, existence_quantity, available_quantity, publishing_year, edition, editorial_id) VALUES('Dummy Book 8', 1, 8, 8, 2008, 2, 2);
INSERT INTO library_db.book (title, active, existence_quantity, available_quantity, publishing_year, edition, editorial_id) VALUES('Dummy Book 9', 0, 9, 9, 2009, 1, 3);
INSERT INTO library_db.book (title, active, existence_quantity, available_quantity, publishing_year, edition, editorial_id) VALUES('Dummy Book 10', 1, 10, 10, 2010, 10, 1);
--rollback

--changeset lisandro.ortega:library_db.book_author.changelog.0.2 context:test
--comment populate table book_author
INSERT INTO library_db.book_author (book_id, author_id) VALUES(1, 1);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(1, 2);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(2, 1);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(2, 2);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(3, 1);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(3, 2);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(4, 1);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(4, 2);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(5, 1);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(5, 2);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(6, 1);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(6, 2);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(7, 1);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(7, 2);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(8, 1);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(8, 2);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(9, 1);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(9, 2);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(10, 1);
INSERT INTO library_db.book_author (book_id, author_id) VALUES(10, 2);
--rollback

--changeset lisandro.ortega:library_db.book_genre.changelog.0.2 context:test
--comment populate table book_genre
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(1, 1);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(1, 2);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(2, 1);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(2, 2);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(3, 1);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(3, 2);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(4, 1);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(4, 2);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(5, 1);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(5, 2);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(6, 1);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(6, 2);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(7, 1);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(7, 2);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(8, 1);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(8, 2);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(9, 1);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(9, 2);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(10, 1);
INSERT INTO library_db.book_genre (book_id, genre_id) VALUES(10, 2);
--rollback
