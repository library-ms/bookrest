--liquibase formatted sql

--changeset lisandro.ortega:library_db.author.changelog.0.1 context:dev,test
--comment create table author
create table author(
    id int primary key AUTO_INCREMENT,
    name text not null
);
--rollback

--changeset lisandro.ortega:library_db.genre.changelog.0.1 context:dev,test
--comment create table genre
create table genre(
	id int primary key AUTO_INCREMENT,
    name text not null,
    description text
);
--rollback

--changeset lisandro.ortega:library_db.editorial.changelog.0.1 context:dev,test
--comment create table editorial
create table editorial(
    id int primary key AUTO_INCREMENT,
    name text not null
);
--rollback

--changeset lisandro.ortega:library_db.book.changelog.0.1 context:dev,test
--comment create table book
create table book(
	id int primary key AUTO_INCREMENT,
    title text not null,
    active TINYINT not null default 1,
    existence_quantity int not null default 0,
	available_quantity int not null default 0,
	publishing_year int,
    edition int,
    editorial_id int,
	CONSTRAINT fk_book_editorial_id FOREIGN key (editorial_id) references editorial(id)
);
--rollback

--changeset lisandro.ortega:library_db.book_genre.changelog.0.1 context:dev,test
--comment create table book_genre to relate books & genres
create table book_genre(
    id int primary key AUTO_INCREMENT,
    book_id int not null,
	genre_id int not null,
	CONSTRAINT fk_book_genre_book_id FOREIGN key (book_id) references book(id),
	CONSTRAINT fk_book_genre_genre_id FOREIGN key (genre_id) references genre(id),
    CONSTRAINT uk_book_genre unique (book_id, genre_id)
);
--rollback

--changeset lisandro.ortega:library_db.book_author.changelog.0.1 context:dev,test
--comment create table book_author to relate books & authors
create table book_author(
    id int primary key AUTO_INCREMENT,
    book_id int not null,
	author_id int not null,
	CONSTRAINT fk_book_author_book_id FOREIGN key (book_id) references book(id),
	CONSTRAINT fk_book_author_author_id FOREIGN key (author_id) references author(id),
    CONSTRAINT uk_book_author unique (book_id, author_id)
);
--rollback
